﻿using DistributedServices.Entities.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DistributedServices.Api.Models
{
	public class ShiftResponse
	{
		public List<Shift> Shifts { get; set; }

		public int Count { get; set; }

		public int Pages { get; set; }
	}
}