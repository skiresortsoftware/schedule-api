﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Infrastructure.CrossCutting.IoC;
using DistributedServices.Entities;
using DistributedServices.Api.Mappings;
using Infrastructure.Data.MainModule.Repositories;
using Infrastructure.Data.MainModule.Models;
using System.Web.Http.Cors;
using DistributedServices.Api.Models;

namespace DistributedServices.Api.Controllers
{
	[EnableCors("*", "*", "*")]
	public class ShiftsController : ApiController
    {
        private readonly IShiftRepository _repository;

        public ShiftsController()
        {
			_repository = new ShiftRepository();
        }

        /// <summary>
        /// All of the items.
        /// </summary>
        /// <returns>All items.</returns>
        [HttpGet]
        public HttpResponseMessage GetAll([FromUri]string clientToken)
        {
            var items = _repository.List(clientToken);

            var itemDto = items.Select(i => Mapper.Map(i));

            return Request.CreateResponse(HttpStatusCode.OK, itemDto);
        }

		[HttpGet]
		public HttpResponseMessage GetAllBySeason([FromUri]string clientToken, [FromUri]int seasonId, [FromUri]int? from, [FromUri]int? to)
		{
			var items = _repository.List(clientToken).Where(s => s.SeasonId == seasonId);

			var totalItems = items.Count();

			if (from != null && to != null)
			{
				var pagedItems = items.Skip(int.Parse(from.ToString())).Take(int.Parse(to.ToString()));

				var pagedItemDto = pagedItems.Select(i => Mapper.Map(i)).ToList();

				var pageSize = int.Parse(to.ToString()) - int.Parse(from.ToString());

				var pages = totalItems / pageSize;

				return Request.CreateResponse(HttpStatusCode.OK, new ShiftResponse
				{
					Shifts = pagedItemDto,
					Count = totalItems,
					Pages = pages
				});
			}

			var itemDto = items.Select(i => Mapper.Map(i));

			return Request.CreateResponse(HttpStatusCode.OK, itemDto);
		}

		[HttpGet]
		public HttpResponseMessage GetAllBySeasonAgg([FromUri]string clientToken, [FromUri]int seasonId)
		{
			var items = _repository.ListAggregates(clientToken, seasonId);

			var itemDto = items.Select(i => Mapper.Map(i));

			return Request.CreateResponse(HttpStatusCode.OK, itemDto);
		}

		[HttpGet]
        public HttpResponseMessage GetEmployeeShifts([FromUri]string clientToken, [FromUri]int employeeId)
        {
            var items = _repository.List(clientToken).Where(e => e.EmployeeId == employeeId);

            var itemDto = items.Select(i => Mapper.Map(i));

            return Request.CreateResponse(HttpStatusCode.OK, itemDto);
        }

        [HttpGet]
        public HttpResponseMessage GetAvailableShifts([FromUri]string clientToken)
        {
            var items = _repository.List(clientToken).Where(e => e.EmployeeId == null);

            var itemDto = items.Select(i => Mapper.Map(i));

            return Request.CreateResponse(HttpStatusCode.OK, itemDto);
        }

        /// <summary>
        /// A item.
        /// </summary>
        /// <param name="id">Unique identifier for an item.</param>
        /// <returns>Employee title.</returns>
        [HttpGet]
        public HttpResponseMessage Get([FromUri]int id, [FromUri]string clientToken)
        {
            var item = _repository.Get(id, clientToken);

            var itemDto = Mapper.Map(item);

            return Request.CreateResponse(HttpStatusCode.OK, itemDto);
        }

        [HttpGet]
        public HttpResponseMessage ListBy([FromUri]string clientToken, [FromUri]int year, [FromUri]int month, [FromUri]int day)
        {
            var item = _repository.GetBy(clientToken, year, month, day);

            var itemDto = item.Select(s => Mapper.Map(s));

            return Request.CreateResponse(HttpStatusCode.OK, itemDto);
        }

        /// <summary>
        /// Creates a new item.
        /// </summary>
        /// <param name="item">New item to create in the given bundle.</param>
        /// <returns>The recently created item.</returns>
        public HttpResponseMessage Post([FromBody]Shift item, [FromUri]string clientToken)
        {
            if (item == null)
                return Request.CreateResponse(HttpStatusCode.OK, new Shift());

			//var itemDtos = new List<DistributedServices.Entities.Dto.Shift>();
			//for (int i = 1; i < item.Multiplier; i++)
			//{
			var itemDto = Mapper.Map(_repository.Add(item, clientToken));

				//itemDtos.Add(itemDto);
			//}

            return Request.CreateResponse(HttpStatusCode.OK, itemDto);
        }

        /// <summary>
        /// Updates an existing item.
        /// </summary>
        /// <param name="id">Unique identifier for the item to update.</param>
        /// <param name="item">Item to update.</param>
        /// <returns>The recently updated item.</returns>
        public HttpResponseMessage Put([FromUri]int id, [FromBody]Shift item, [FromUri]string clientToken)
        {
            if (item == null)
                return Request.CreateResponse(HttpStatusCode.OK, new Shift());

            item.Id = id;

            var itemDto = Mapper.Map(_repository.Update(item, clientToken));

            return Request.CreateResponse(HttpStatusCode.OK, itemDto);
        }

        /// <summary>
        /// Deletes an existing item.
        /// </summary>
        /// <param name="id">Unique identifier for an item.</param>
        /// <returns>The recently deleted item.</returns>
        public HttpResponseMessage Delete([FromUri]int id, [FromUri]string clientToken)
        {
            var itemDto = Mapper.Map(_repository.Delete(id, clientToken));

            return Request.CreateResponse(HttpStatusCode.OK, itemDto);
        }

		/// <summary>
		/// Deletes an existing item.
		/// </summary>
		/// <param name="id">Unique identifier for an item.</param>
		/// <returns>The recently deleted item.</returns>
		public HttpResponseMessage PostDelCors([FromUri]int id, [FromUri]string clientToken, [FromUri]bool delete)
		{
			var itemDto = Mapper.Map(_repository.Delete(id, clientToken));

			return Request.CreateResponse(HttpStatusCode.OK, itemDto);
		}

		/// <summary>
		/// Deletes an existing item.
		/// </summary>
		/// <param name="id">Unique identifier for an item.</param>
		/// <returns>The recently deleted item.</returns>
		public HttpResponseMessage PostDelCorsByDate([FromUri]DateTime date, [FromUri]int seasonId, [FromUri]int typeId, [FromUri]string clientToken, [FromUri]bool delete)
		{
			var itemDtos = Mapper.Map(_repository.DeleteMany(date, seasonId, typeId, clientToken));

			return Request.CreateResponse(HttpStatusCode.OK, itemDtos);
		}
	}
}
