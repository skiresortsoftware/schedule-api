﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Infrastructure.CrossCutting.IoC;
using DistributedServices.Entities;
using DistributedServices.Api.Mappings;
using Infrastructure.Data.MainModule.Repositories;
using Infrastructure.Data.MainModule.Models;
using System.Web.Http.Cors;

namespace DistributedServices.Api.Controllers
{
	[EnableCors("*", "*", "*")]
	public class SeasonsController : ApiController
    {
        private readonly ISeasonRepository _repository;

        public SeasonsController()
        {
			_repository = new SeasonRepository();
        }

        /// <summary>
        /// All of the items.
        /// </summary>
        /// <returns>All items.</returns>
        public HttpResponseMessage GetAll([FromUri]string clientToken, bool? current)
        {
            var items = _repository.List(clientToken);

			if (current != null)
				items = items.Where(c => c.Current == current);

            var itemDto = items.Select(i => Mapper.Map(i));

            return Request.CreateResponse(HttpStatusCode.OK, itemDto);
        }

        /// <summary>
        /// A item.
        /// </summary>
        /// <param name="id">Unique identifier for an item.</param>
        /// <returns>Employee title.</returns>
        public HttpResponseMessage Get([FromUri]int id, [FromUri]string clientToken)
        {
            var item = _repository.Get(id, clientToken);

            var itemDto = Mapper.Map(item);

            return Request.CreateResponse(HttpStatusCode.OK, itemDto);
        }

		/// <summary>
		/// A item.
		/// </summary>
		/// <param name="id">Unique identifier for an item.</param>
		/// <returns>Employee title.</returns>
		//public HttpResponseMessage GetCurrent([FromUri]bool current, [FromUri]string clientToken)
		//{
		//	var items = _repository.List(clientToken).Where(c => c.Current == current);

		//	var itemDto = items.Select(i => Mapper.Map(i));

		//	return Request.CreateResponse(HttpStatusCode.OK, itemDto);
		//}

		/// <summary>
		/// Creates a new item.
		/// </summary>
		/// <param name="item">New item to create in the given bundle.</param>
		/// <returns>The recently created item.</returns>
		public HttpResponseMessage Post([FromBody]Season item, [FromUri]string clientToken)
        {
            if (item == null)
                return Request.CreateResponse(HttpStatusCode.OK, new Season());

			if (item.Current)
			{
				var seasons = _repository.List(clientToken).ToList();

				foreach (var season in seasons)
				{
					var seasonToUpdate = season;

					seasonToUpdate.Shifts = null;

					seasonToUpdate.Current = false;

					_repository.Update(seasonToUpdate, clientToken);
				}
			}

			var itemDto = Mapper.Map(_repository.Add(item, clientToken));

            return Request.CreateResponse(HttpStatusCode.OK, itemDto);
        }

        /// <summary>
        /// Updates an existing item.
        /// </summary>
        /// <param name="id">Unique identifier for the item to update.</param>
        /// <param name="item">Item to update.</param>
        /// <returns>The recently updated item.</returns>
        public HttpResponseMessage Put([FromUri]int id, [FromBody]Season item, [FromUri]string clientToken)
        {
            if (item == null)
                return Request.CreateResponse(HttpStatusCode.OK, new Season());

            item.Id = id;

			if (item.Current)
			{
				var seasons = _repository.List(clientToken).ToList();

				foreach (var season in seasons)
				{
					var seasonToUpdate = season;

					seasonToUpdate.Shifts = null;

					seasonToUpdate.Current = false;

					_repository.Update(seasonToUpdate, clientToken);
				}
			}

			var itemDto = Mapper.Map(_repository.Update(item, clientToken));

            return Request.CreateResponse(HttpStatusCode.OK, itemDto);
        }

		/// <summary>
		/// Updates an existing item.
		/// </summary>
		/// <param name="id">Unique identifier for the item to update.</param>
		/// <param name="item">Item to update.</param>
		/// <returns>The recently updated item.</returns>
		public HttpResponseMessage Post([FromUri]int id, [FromBody]Season item, [FromUri]string clientToken, [FromUri]bool update)
		{
			if (item == null)
				return Request.CreateResponse(HttpStatusCode.OK, new Season());

			item.Id = id;

			if (item.Current)
			{
				var seasons = _repository.List(clientToken).ToList();

				foreach (var season in seasons)
				{
					var seasonToUpdate = season;

					seasonToUpdate.Shifts = null;

					seasonToUpdate.Current = false;

					_repository.Update(seasonToUpdate, clientToken);
				}
			}

			var itemDto = Mapper.Map(_repository.Update(item, clientToken));

			return Request.CreateResponse(HttpStatusCode.OK, itemDto);
		}

		/// <summary>
		/// Deletes an existing item.
		/// </summary>
		/// <param name="id">Unique identifier for an item.</param>
		/// <returns>The recently deleted item.</returns>
		public HttpResponseMessage Delete([FromUri]int id, [FromUri]string clientToken)
        {
            var itemDto = Mapper.Map(_repository.Delete(id, clientToken));

            return Request.CreateResponse(HttpStatusCode.OK, itemDto);
        }

		/// <summary>
		/// Deletes an existing item.
		/// </summary>
		/// <param name="id">Unique identifier for an item.</param>
		/// <returns>The recently deleted item.</returns>
		public HttpResponseMessage DeleteCors([FromUri]int id, [FromUri]string clientToken, bool delete)
		{
			var itemDto = Mapper.Map(_repository.Delete(id, clientToken));

			return Request.CreateResponse(HttpStatusCode.OK, itemDto);
		}
	}
}
