﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DistributedServices.Api.Mappings
{
    public static class Mapper
    {
		public static List<Entities.Dto.Shift> Map(List<Infrastructure.Data.MainModule.Models.Shift> items)
		{
			if (items == null)
				return new List<Entities.Dto.Shift>();

			var mappedItems = new List<Entities.Dto.Shift>();
			foreach (var item in items)
				mappedItems.Add(Mapper.Map(item));

			return mappedItems;
		}

        public static Entities.Dto.Shift Map(Infrastructure.Data.MainModule.Models.Shift item)
        {
            if (item == null)
                return new Entities.Dto.Shift();
            
            var dto = new Entities.Dto.Shift()
            {
                ClientId = item.ClientId,
                Id = item.Id,
                EmployeeId = item.EmployeeId,
                Date = item.Date,
                Start = item.Start,
                End = item.End,
                SeasonId = item.SeasonId,
                PriorityId = item.PriorityId,
                ShiftTypeId = item.TypeId,
                Season = Mapper.Map(item.Season),
                Priority = Mapper.Map(item.Priority),
                ShiftType = Mapper.Map(item.ShiftType)
            };

            return dto;
        }

		public static Entities.Dto.ShiftAggregate Map(Infrastructure.Data.MainModule.Models.ShiftAggregate item)
		{
			if (item == null)
				return new Entities.Dto.ShiftAggregate();

			var dto = new Entities.Dto.ShiftAggregate()
			{
				Date = item.Date,
				TypeId = item.TypeId,
				TypeName = item.TypeName,
				TypeDescription = item.TypeDescription,
				SeasonName = item.SeasonName,
				SeasonDescription = item.SeasonDescription,
				Start = item.Start,
				End = item.End,
				SeasonId = item.SeasonId,
				PriorityId = item.PriorityId,
				PriorityDescription = item.PriorityDescription,
				PriorityName = item.PriorityName,
				Total = item.Total
			};

			return dto;
		}

		public static DistributedServices.Entities.Dto.Season Map(Infrastructure.Data.MainModule.Models.Season item)
        {
            if (item == null)
                return new Entities.Dto.Season();
            
            var dto = new Entities.Dto.Season()
            {
                Name = item.Name,
                Description = item.Description,
                Start = item.Start,
                End = item.End,
				Current = item.Current,
                Id = item.Id
            };

            return dto;
        }

        public static DistributedServices.Entities.Dto.Priority Map(Infrastructure.Data.MainModule.Models.Priority item)
        {
            if (item == null)
                return new Entities.Dto.Priority();
            
            var dto = new Entities.Dto.Priority()
            {
                Name = item.Name,
                Description = item.Description,
                Id = item.Id
            };

            return dto;
        }

        public static DistributedServices.Entities.Dto.ShiftType Map(Infrastructure.Data.MainModule.Models.ShiftType item)
        {
            if (item == null)
                return new Entities.Dto.ShiftType();
            
            var dto = new Entities.Dto.ShiftType()
            {
                Name = item.Name,
                Description = item.Description,
                Id = item.Id
            };

            return dto;
        }

        public static DistributedServices.Entities.Dto.ShiftTime Map(Infrastructure.Data.MainModule.Models.ShiftTime item)
        {
            if (item == null)
                return new Entities.Dto.ShiftTime();

            var dto = new Entities.Dto.ShiftTime()
            {
                Name = item.Name,
                Description = item.Description,
                Start = item.Start,
                End = item.End,
                Id = item.Id
            };

            return dto;
        }
    }
}