﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistributedServices.Entities.Dto
{
	public class ShiftAggregate
	{
		public DateTime Date { get; set; }
		public DateTime Start { get; set; }
		public DateTime End { get; set; }
		public int TypeId { get; set; }
		public string TypeName { get; set; }
		public string TypeDescription { get; set; }
		public int SeasonId { get; set; }
		public string SeasonName { get; set; }
		public string SeasonDescription { get; set; }
		public int PriorityId { get; set; }
		public string PriorityName { get; set; }
		public string PriorityDescription { get; set; }
		public int Total { get; set; }
	}
}
