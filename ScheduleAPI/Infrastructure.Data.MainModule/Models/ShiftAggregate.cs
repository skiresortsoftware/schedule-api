﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Infrastructure.Data.MainModule.Models
{
	public class ShiftAggregate
	{
		[Column(Order = 0), Key]
		public DateTime Date { get; set; }
		[Column(Order = 1), Key]
		public DateTime Start { get; set; }
		[Column(Order = 2), Key]
		public DateTime End { get; set; }
		[Column(Order = 3), Key]
		public int TypeId { get; set; }
		public string TypeName { get; set; }
		public string TypeDescription { get; set; }
		public int SeasonId { get; set; }
		public string SeasonName { get; set; }
		public string SeasonDescription { get; set; }
		public int PriorityId { get; set; }
		public string PriorityName { get; set; }
		public string PriorityDescription { get; set; }
		public int Total { get; set; }
    }
}
