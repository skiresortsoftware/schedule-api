﻿using System;
using System.Collections.Generic;
using System.Linq;
using Infrastructure.Data.MainModule.Models;
using Domain.Core;
using System.Data.Entity.Validation;

namespace Infrastructure.Data.MainModule.Repositories
{
    public class ShiftRepository : IShiftRepository
    {
        private readonly ScheduleApiContext _context;

        public ShiftRepository()
        {
            _context = new ScheduleApiContext();
        }

        public Shift Add(Shift item, string clientToken)
        {
            var guid = Guid.Parse(clientToken);

            item.ClientToken = guid;

			var multiplier = item.Multiplier ?? 1;


			var addedItems = new List<Shift>();
			for(var s = 1; s <= multiplier; s++)
			{
				var shiftToAdd = new Shift();

				shiftToAdd.Assigned = item.Assigned;
				shiftToAdd.CanAdd = item.CanAdd;
				shiftToAdd.CanRemove = item.CanRemove;
				shiftToAdd.CanUpdate = item.CanUpdate;
				shiftToAdd.ClientId = item.ClientId;
				shiftToAdd.ClientToken = item.ClientToken;
				shiftToAdd.Date = item.Date;
				shiftToAdd.EmployeeId = item.EmployeeId;
				shiftToAdd.End = item.End;
				shiftToAdd.PriorityId = item.PriorityId;
				shiftToAdd.SeasonId = item.SeasonId;
				shiftToAdd.Start = item.Start;
				shiftToAdd.TypeId = item.TypeId;

				var addedItem = _context.Shifts.Add(shiftToAdd);

				addedItem.DateCreated = DateTime.Now;

				addedItems.Add(addedItem);

				_context.SaveChanges();
			}

            return addedItems.FirstOrDefault();
        }

        public Shift Update(Shift item, string clientToken)
        {
            var guid = Guid.Parse(clientToken);

            var itemToUpdate = _context.Shifts.FirstOrDefault(b => b.Id == item.Id && b.ClientToken == guid);

            itemToUpdate.EmployeeId = item.EmployeeId;

            itemToUpdate.DateModified = DateTime.Now;

            _context.SaveChanges();

            return itemToUpdate;
        }

        public Shift Delete(int id, string clientToken)
        {
            var guid = Guid.Parse(clientToken);

            var itemToDelete = _context.Shifts.FirstOrDefault(b => b.Id == id && b.ClientToken == guid);

            var deletedItem = _context.Shifts.Remove(itemToDelete);

            _context.SaveChanges();

            return deletedItem;
        }

		public List<Shift> DeleteMany(DateTime date, int seasonId, int typeId, string clientToken)
		{
			var guid = Guid.Parse(clientToken);

			var itemToDeletes = _context.Shifts.Where(b => b.Date == date && b.SeasonId == seasonId && b.ClientToken == guid && b.TypeId == typeId);

			var deletedItems = _context.Shifts.RemoveRange(itemToDeletes);

			_context.SaveChanges();

			return deletedItems.ToList();
		}

        public IEnumerable<Shift> List(string clientToken)
        {
            var guid = Guid.Parse(clientToken);

            var items = _context.Shifts.Where(b => b.ClientToken == guid);

            return items;
        }

		public IEnumerable<ShiftAggregate> ListAggregates(string clientToken, int seasonId)
		{
			var guid = Guid.Parse(clientToken);

			var items = _context.ShiftAggregates.Where(s => s.SeasonId == seasonId);

			return items;
		}

        public Shift Get(int id, string clientToken)
        {
            var guid = Guid.Parse(clientToken);

            var item = _context.Shifts.ToList().FirstOrDefault(b => b.Id == id && b.ClientToken == guid);

            return item;
        }


        public IEnumerable<Shift> GetBy(string clientToken, int year, int month, int day)
        {
            var guid = Guid.Parse(clientToken);

            var items = _context.Shifts.Where(b => b.ClientToken == guid &&
                                                   b.Date.Year == year &&
                                                   b.Date.Month == month &&
                                                   b.Date.Day == day);

            return items;
        }
    }
}
