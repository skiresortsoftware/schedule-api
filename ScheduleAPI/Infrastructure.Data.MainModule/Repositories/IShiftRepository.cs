﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Infrastructure.Data.MainModule.Models;
namespace Infrastructure.Data.MainModule.Repositories
{
    public interface IShiftRepository
    {
        Shift Add(Shift item, string clientToken);
        Shift Delete(int id, string clientToken);
		List<Shift> DeleteMany(DateTime date, int seasonId, int typeId, string clientToken);
        Shift Get(int id, string clientToken);
        IEnumerable<Shift> GetBy(string clientToken, int year, int month, int day);
        IEnumerable<Shift> List(string clientToken);
		IEnumerable<ShiftAggregate> ListAggregates(string clientToken, int seasonId);
        Shift Update(Shift item, string clientToken);
    }
}
